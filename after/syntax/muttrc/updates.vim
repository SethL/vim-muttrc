""" changes since 1.10.0

" 1.11.0
"	+ <check-stats> manually updates mailbox statistics, the same way $mail_check_stats does when set.
"	+ <compose-to-sender> composes a message to the sender of the selected message, in the index or attachment menu.
syn match muttrcFunction contained /\<\%(check-stats\|compose-to-sender\)\>/
"	+ index-format-hook and the new %@name@ expando for $index_format enable dynamic index formats
syn match muttrcIndexFormatEscapes contained /%\%(\%(-\?[0-9]\+\)\?\%(\.[0-9]\+\)\?\)\?[:_]\?@[^@]\+@/
syn keyword muttrcHooks contained skipwhite index-format-hook
syn match muttrcPatHooks /\<index-format-hook\>/ skipwhite nextgroup=muttrcIFHookName
syn match muttrcIFHookName contained /\w\+/ skipwhite nextgroup=muttrcPatHookNot,muttrcOptPattern
hi def link muttrcIFHookName muttrcVariableInner
"	! Relative date matching allows hour, minute, and second units: HMS.
syn match muttrcSimplePat contained "!\?\^\?[~][dr]\s*\%(\%(-\?[0-9]\{1,2}\%(/[0-9]\{1,2}\%(/[0-9]\{2}\%([0-9]\{2}\)\?\)\?\)\?\%([+*-][0-9]\+[HMS]\)*\)\|\%(\%([0-9]\{1,2}\%(/[0-9]\{1,2}\%(/[0-9]\{2}\%([0-9]\{2}\)\?\)\?\)\?\%([+*-][0-9]\+[HMS]\)*\)-\%([0-9]\{1,2}\%(/[0-9]\{1,2}\%(/[0-9]\{2}\%([0-9]\{2}\)\?\)\?\)\?\%([+*-][0-9]\+[HMS]\)\?\)\?\)\|\%([<>=][0-9]\+[HMS]\)\|\%(`[^`]\+`\)\|\%(\$[a-zA-Z0-9_-]\+\)\)" contains=muttrcShellString,muttrcVariable

" 1.12.0
"	+ <descend-directory> in the browser menu allows entering nested maildir directories.
"	+ <group-chat-reply> replies to all, but preserves To recipients in the reply.
syn match muttrcFunction contained /\<\%(descend-directory\|group-chat-reply\)\>/

" 1.13.0
"	! <display-filename> in the browser menu shows the full path for local and IMAP mailboxes.
"	+ <browse-mailboxes> allows direct access to the mailboxes list from the index and pager, without having to use a macro.
"	+ In the compose menu <view-alt>, <view-alt-text>, <view-alt-mailcap> allow previewing the output of the $send_multipart_alternative_filter.
syn match muttrcFunction contained /\<\%(display-filename\|browse-mailboxes\|view-alt\%(-text\|-mailcap\)\?\)\>/
"	! unattachments now has a '*' parameter to remove all attachment counting.
syn keyword muttrcAttachmentsFlag contained *

" 1.14.0
"	! Date pattern modifiers accept YYYYMMDD format dates.
syn match muttrcSimplePat contained "!\?\^\?[~][dr]\s*\%(\%([0-9]\{8}\%([+*-][0-9]\+[ymwd]\)\?\)-\%([0-9]\{8}\%([+*-][0-9]\+[ymwd]\)\?\)\?\|\%(-\?[0-9]\{8}\%([+*-][0-9]\+[ymwd]\)\?\)\)" contains=muttrcShellString,muttrcVariable
"	+ New compose menu functions <move-up> and <move-down> to rearrange the order parts.
"	+ Background editing support; New function <background-compose-menu> in the index and pager menu.
"	+ Index and pager menu functions <sidebar-first> and <sidebar-last>.
syn match muttrcFunction contained /\<\%(move-\%(up\|down\)\|background-compose-menu\|sidebar-\%(first\|last\)\)\>/
"	+ Background editing support; New %B expando added to the default for $status_format.
syn match muttrcStatusFormatEscapes contained /%\%(\%(-\?[0-9]\+\)\?\%(\.[0-9]\+\)\?\)\?[:_]\?[B]/

" 2.0.0
"	+ Domain-literal support for email addresses, e.g user@[IPv6:fcXX:...]
syn match muttrcEmail "[a-zA-Z0-9._-]\+@\[IPv6:[a-fA-F0-9:]\+\]"
syn match muttrcEmail "[a-zA-Z0-9._-]\+@\[[0-9.]\+\]"
"	+ The "cd" command allows changing the current working directory.
syn keyword muttrcCommand skipwhite cd
