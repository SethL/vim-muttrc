" updated 'color' syntax (1.10.0 --> 1.13.1)

" 1.12.0
"	! Color names can be prefixed with 'light' in addition to 'bright'.
"	  'bright' colors are bold face, while 'light' are non-bold.
syn keyword muttrcColor contained lightblack lightblue lightcyan lightdefault lightgreen lightmagenta lightred lightwhite lightyellow
syn match   muttrcColor contained "\<\%(light\)\=color\d\{1,3}\>"


"	! Color commands can now include an attribute (e.g. bold, underline).
syn match muttrcColorAttrib contained /\s*\%(none\|bold\|underline\|reverse\|standout\)\?/ nextgroup=muttrcColorFG,muttrcColorFGNL
syn match muttrcColorAttribNL contained skipnl "\s*\\$" nextgroup=muttrcColorAttrib,muttrcColorAttribNL
syn clear muttrcColorContext
syn match muttrcColorContext contained /\s*[$]\?\w\+/ contains=muttrcColorField,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorAttrib,muttrcColorAttribNL

syn match muttrcColorAttribI contained /\s*\%(none\|bold\|underline\|reverse\|standout\)\?/ nextgroup=muttrcColorFGI,muttrcColorFGNLI
syn match muttrcColorAttribNLI contained skipnl "\s*\\$" nextgroup=muttrcColorAttribI,muttrcColorAttribNLI
syn clear muttrcColorContextI
syn match muttrcColorContextI contained /\s*\<index\>/ contains=muttrcUnHighlightSpace nextgroup=muttrcColorAttribI,muttrcColorAttribNLI

syn match muttrcColorAttribH contained /\s*\%(none\|bold\|underline\|reverse\|standout\)\?/ nextgroup=muttrcColorFGH,muttrcColorFGNLH
syn match muttrcColorAttribNLH contained skipnl "\s*\\$" nextgroup=muttrcColorAttribH,muttrcColorAttribNLH
syn clear muttrcColorContextH
syn match muttrcColorContextH contained /\s*\<\%(header\|body\)\>/ contains=muttrcUnHighlightSpace nextgroup=muttrcColorAttribH,muttrcColorAttribNLH

" compose menu coloring options (1.9.0)
syn match muttrcColorBGC	contained /\s*[$]\?\w\+\s*/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcPattern,muttrcPatternNL
syn match muttrcColorBGNLC	contained skipnl "\s*\\$" nextgroup=muttrcColorBGC,muttrcColorBGNLC
syn match muttrcColorFGC	contained /\s*[$]\?\w\+/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorBGC,muttrcColorBGNLC
syn match muttrcColorFGNLC	contained skipnl "\s*\\$" nextgroup=muttrcColorFGC,muttrcColorFGNLC
syn match muttrcColorAttribC	contained /\s*\%(none\|bold\|underline\|reverse\|standout\)\?/ nextgroup=muttrcColorFGC,muttrcColorFGNLC
syn match muttrcColorAttribNLC	contained skipnl "\s*\\$" nextgroup=muttrcColorAttribC,muttrcColorAttribNLC
syn keyword muttrcColorComposeObject skipwhite header security_encrypt security_sign security_both security_none nextgroup=muttrcColorFGC,muttrcColorFGNLC
syn match muttrcColorContextC contained /\s*\<compose\>\s\+\w\+/ contains=muttrcColorComposeObject nextgroup=muttrcColorAttribC,muttrcColorAttribNLC
syn match muttrcColorNLC contained skipnl "\s*\\$" nextgroup=muttrcColorContextC,muttrcColorNLC
syn match muttrcColorKeywordC contained skipwhite /\<color\>/ nextgroup=muttrcColorContextC,muttrcColorNLC

" need to replace muttrcColorLine to add "compose"
syn clear muttrcColorLine
syn region muttrcColorLine keepend start=/^\s*color\s\+\%(index\|header\|compose\)\@!/ skip=+\\$+ end=+$+ contains=muttrcColorKeyword,muttrcComment,muttrcUnHighlightSpace
syn region muttrcColorLine keepend skipwhite start=/\<color\s\+index\>/ skip=+\\$+ end=+$+ contains=muttrcColorKeywordI,muttrcComment,muttrcUnHighlightSpace
syn region muttrcColorLine keepend skipwhite start=/\<color\s\+header\>/ skip=+\\$+ end=+$+ contains=muttrcColorKeywordH,muttrcComment,muttrcUnHighlightSpace
syn region muttrcColorLine keepend skipwhite start=/\<color\s\+compose\>/ skip=+\\$+ end=+$+ contains=muttrcColorKeywordC,muttrcComment,muttrcUnHighlightSpace


hi def link muttrcColorAttrib		muttrcColor
hi def link muttrcColorAttribI		muttrcColor
hi def link muttrcColorAttribH		muttrcColor
hi def link muttrcColorAttribC		muttrcColor

hi def link muttrcColorAttribNL		SpecialChar
hi def link muttrcColorAttribNLI	SpecialChar
hi def link muttrcColorAttribNLH	SpecialChar
hi def link muttrcColorAttribNLC	SpecialChar

hi def link muttrcColorContextC		Identifier
hi def link muttrcColorKeywordC		muttrcColorKeyword
hi def link muttrcColorFGC		Error
hi def link muttrcColorBGC		Error
hi def link muttrcColorComposeObject	Identifier


