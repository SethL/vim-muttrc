" syntax fixes and updates

" fix: allow digits in variable names (e.g. set my_foo = $use_ipv6)
syn match muttrcVariable	contained "\\\@<!\w*\$\w\+" contains=muttrcVariableInner
syn match muttrcVariableInner	contained "\$\w\+"
syn match muttrcEscapedVariable	contained "\\\$\w\+"

" fix: variable expansions can be in curly braces
syn match muttrcVariable	contained "\\\@<!\w*\${\w\+}" contains=muttrcVariableInner
syn match muttrcVariableInner	contained "\${\w\+}"
syn match muttrcEscapedVariable	contained "\\\${\w\+}"
syn match muttrcSetStrAssignment contained skipwhite /=\s*\%(\\\?\$\)\?{\w\+/hs=s+1 nextgroup=muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr contains=muttrcVariable,muttrcEscapedVariable
syn match muttrcColorBG 	contained /\s*\${\w\+}/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorRXPat,muttrcColorRXNL
syn match muttrcColorFG 	contained /\s*\${\w\+}/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorBG,muttrcColorBGNL
syn match muttrcColorContext 	contained /\s*\${\w\+}/ contains=muttrcColorField,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorFG,muttrcColorFGNL
syn match muttrcColorBGI	contained /\s*\${\w\+}\s*/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcPattern,muttrcPatternNL
syn match muttrcColorFGI	contained /\s*\${\w\+}/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorBGI,muttrcColorBGNLI
syn match muttrcColorBGH	contained /\s*\${\w\+}\s*/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcRXString,muttrcRXPatternNL
syn match muttrcColorFGH	contained /\s*\${\w\+}/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorBGH,muttrcColorBGNLH
" same updates for after/.../color.vim
syn match muttrcColorContext contained /\s*[$]{\w\+}/ contains=muttrcColorField,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorAttrib,muttrcColorAttribNL
syn match muttrcColorBGC	contained /\s*[$]{\w\+}\s*/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcPattern,muttrcPatternNL
syn match muttrcColorFGC	contained /\s*[$]{\w\+}/ contains=muttrcColor,muttrcVariable,muttrcUnHighlightSpace nextgroup=muttrcColorBGC,muttrcColorBGNLC

" fix: ungroup can be '*'
syn region muttrcUngroupLine keepend start=+^\s*ungroup\s+ skip=+\\$+ end=+$+ contains=muttrcGroupKeyword,muttrcGroupDef,muttrcAsterisk,muttrcUnHighlightSpace,muttrcComment

" fix: size can have [KM] suffix, (case insensitive, not just [kM])
syn match muttrcSimplePat contained "!\?\^\?[~][mnXz]\s*\%([<>-][0-9]\+[KkMm]\?\|[0-9]\+[KkMm]\?[-]\%([0-9]\+[KkMm]\?\)\?\)"


