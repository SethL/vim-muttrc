""" new variables (1.10.0 --> 2.0.0)

" ----- boolean

syn keyword muttrcVarBool	skipwhite contained
	\ pgp_check_gpg_decrypt_status_fd
	\ imap_condstore imap_qresync
	\ auto_subscribe
	\ crypt_protected_headers_read crypt_protected_headers_save crypt_protected_headers_write
	\ include_encrypted
	\ fcc_before_send
	\ size_show_bytes size_show_fractions size_show_mb size_units_on_left
	\ ssl_use_tlsv1_1 ssl_use_tlsv1_2 ssl_use_tlsv1_3
	\ browser_sticky_cursor
	\ count_alternatives
	\ sidebar_relative_shortpath_indent sidebar_use_mailbox_shortcuts
	\ imap_deflate 
	\ crypt_opportunistic_encrypt_strong_keys
	\ background_edit background_confirm_quit
	\ tunnel_is_secure
	\ copy_decode_weed pipe_decode_weed print_decode_weed
	\ cursor_overlay
	\ nextgroup=muttrcSetBoolAssignment,muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

syn keyword muttrcVarBool	skipwhite contained
	\ nopgp_check_gpg_decrypt_status_fd
	\ noimap_condstore noimap_qresync
	\ noauto_subscribe
	\ nocrypt_protected_headers_read nocrypt_protected_headers_save nocrypt_protected_headers_write
	\ noinclude_encrypted
	\ nofcc_before_send
	\ nosize_show_bytes nosize_show_fractions nosize_show_mb nosize_units_on_left
	\ nossl_use_tlsv1_1 nossl_use_tlsv1_2 nossl_use_tlsv1_3
	\ nobrowser_sticky_cursor
	\ nocount_alternatives
	\ nosidebar_relative_shortpath_indent nosidebar_use_mailbox_shortcuts
	\ noimap_deflate
	\ nocrypt_opportunistic_encrypt_strong_keys
	\ nobackground_edit nobackground_confirm_quit
	\ notunnel_is_secure
	\ nocopy_decode_weed nopipe_decode_weed noprint_decode_weed
	\ nocursor_overlay
	\ nextgroup=muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

syn keyword muttrcVarBool	skipwhite contained
	\ invpgp_check_gpg_decrypt_status_fd
	\ invimap_condstore invimap_qresync
	\ invauto_subscribe
	\ invcrypt_protected_headers_read invcrypt_protected_headers_save invcrypt_protected_headers_write
	\ invinclude_encrypted
	\ invfcc_before_send
	\ invsize_show_bytes invsize_show_fractions invsize_show_mb invsize_units_on_left
	\ invssl_use_tlsv1_1 invssl_use_tlsv1_2 invssl_use_tlsv1_3
	\ invbrowser_sticky_cursor
	\ invcount_alternatives
	\ invsidebar_relative_shortpath_indent invsidebar_use_mailbox_shortcuts
	\ invimap_deflate
	\ invcrypt_opportunistic_encrypt_strong_keys
	\ invbackground_edit invbackground_confirm_quit
	\ invtunnel_is_secure
	\ invcopy_decode_weed invpipe_decode_weed invprint_decode_weed
	\ invcursor_overlay
	\ nextgroup=muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

" ----- quadoption

syn keyword muttrcVarQuad	skipwhite contained
	\ forward_attachments
	\ send_multipart_alternative
	\ nextgroup=muttrcSetQuadAssignment,muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

syn keyword muttrcVarQuad	skipwhite contained
	\ noforward_attachments
	\ nosend_multipart_alternative
	\ nextgroup=muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

syn keyword muttrcVarQuad	skipwhite contained
	\ invforward_attachments
	\ invsend_multipart_alternative
	\ nextgroup=muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

" ----- number

syn keyword muttrcVarNum	skipwhite contained
	\ imap_fetch_chunk_size
	\ nextgroup=muttrcSetNumAssignment,muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

" ----- string

syn keyword muttrcVarStr	contained skipwhite
	\ imap_oauth_refresh_command pop_oauth_refresh_command smtp_oauth_refresh_command
	\ crypt_protected_headers_subject
	\ send_multipart_alternative_filter
	\ fcc_delimiter background_format
	\ attach_save_dir
	\ nextgroup=muttrcSetStrAssignment,muttrcVPrefix,muttrcVarBool,muttrcVarQuad,muttrcVarNum,muttrcVarStr

